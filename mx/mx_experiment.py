# Moved from main cristallina.py here temporarily
mxdaq = SFAcquisition(
    instrument,
    pgroup,
    default_channels=bs_channels,
    default_pvs=pvs,
    default_detectors=detectors_MX,
    rate_multiplicator=1,
    append_user_tag_to_data_dir=True
)

mxscan = Scanner(default_acquisitions=[mxdaq], condition=check_intensity_gas_monitor)

mxgui = GUI(mxscan, show_goto=True, show_spec=False, show_scan=False, show_scan2D=False, show_run=False, show_static=False, show_sfx=True, start_tab="sfx")

############## MX motors ##############
# hve positioners
hve_mot_v = Motor("SARES30-MOBI1:MOT_1")
hve_mot_h1 = Motor("SARES30-MOBI1:MOT_2")
hve_mot_h2 = Motor("SARES30-MOBI1:MOT_3")

# collimator
coll_x = Motor("SARES30-SMX:MCS1")
coll_y = Motor("SARES30-SMX:MCS2")

# post-tube
pt_x1 = Motor("SARES30-SMX:MCS4")
pt_x2 = Motor("SARES30-SMX:MCS5")
pt_y1 = Motor("SARES30-SMX:MCS6")
pt_y2 = Motor("SARES30-SMX:MCS7")
pt_z = Motor("SARES30-SMX:MCS8")

# post-tube
detector_z = Motor("SAR-EXPMX:MOT_DET_Z")

# post-tube
backlight = Motor("SAR-EXPMX:MOT_BLGT")

############## in positions ##############
coll_in_pos_x, coll_in_pos_y = 9.51, -5.62 
backlight_in = -30000
detector_in_pos = 1
pt_in_pos_x1, pt_in_pos_x2, pt_in_pos_y1, pt_in_pos_y2, pt_in_pos_z = -3.039, -2.637, 8.904, 13.857, 0


############## out positions ##############
coll_out_pos_x, coll_out_pos_y = -14.5, 1.32
backlight_out = 1000
detector_out_pos = 180
pt_out_pos_x1, pt_out_pos_x2, pt_out_pos_y1, pt_out_pos_y2, pt_out_pos_z = 5.960, 6.361, -15.096, -10.143, -8 

@as_shortcut
def a_data_collection():

    # move backlight up
    backlight.set( backlight_out ).wait()

    # post-tube in
    pt_x1_in = pt_x1.set( pt_in_pos_x1 ) # this runs in parallel
    pt_x2_in = pt_x2.set( pt_in_pos_x2 ) # this runs in parallel
    pt_y1_in = pt_y1.set( pt_in_pos_y1 ) # this runs in parallel
    pt_y2_in = pt_y2.set( pt_in_pos_y2 ) # this runs in parallel
    pt_z_in = pt_z.set( pt_in_pos_z ) # this runs in parallel
    for t in (pt_x1_in, pt_x2_in, pt_y1_in, pt_y2_in, pt_z_in): # this waits for all of them to be done!
        t.wait()

    # collimator in
    cx_in = coll_x.set( coll_in_pos_x ) # this runs in parallel
    cy_in = coll_y.set( coll_in_pos_y ) # this runs in parallel
    for t in (cx_in, cy_in): # this waits for all of them to be done!
        t.wait()

    # detector in
    detector_z.set( detector_in_pos ).wait() # this runs in parallel

@as_shortcut
def b_sample_alignment():

    # detector out
    detector_z.set( detector_out_pos ).wait()

    # collimator out
    cx_out = coll_x.set( coll_out_pos_x ) # this runs in parallel
    cy_out = coll_y.set( coll_out_pos_y ) # this runs in parallel
    for t in (cx_out, cy_out): # this waits for all of them to be done!
        t.wait()

    # post-tube out
    pt_x1_out = pt_x1.set( pt_out_pos_x1 ) # this runs in parallel
    pt_x2_out = pt_x2.set( pt_out_pos_x2 ) # this runs in parallel
    pt_y1_out = pt_y1.set( pt_out_pos_y1 ) # this runs in parallel
    pt_y2_out = pt_y2.set( pt_out_pos_y2 ) # this runs in parallel
    pt_z_out = pt_z.set( pt_out_pos_z ) # this runs in parallel
    for t in (pt_x1_out, pt_x2_out, pt_y1_out, pt_y2_out, pt_z_out): # this waits for all of them to be done!
        t.wait()

@as_shortcut
def c_backlight_in():

    # safety logic for backlight in
    if coll_x.get() < 0 and pt_y1.get() < 0 and detector_z.get() > 20:
         backlight.set( backlight_in ).wait()
    else:
        print( "devises are in the way" )

@as_shortcut
def ca_backlight_out():
    backlight.set( backlight_out ).wait()

@as_shortcut
def post_tube_in():

    # safety logic for backlight in
    if backlight.get() > 0 and detector_z.get() > 20:
        pt_x1_in = pt_x1.set( pt_in_pos_x1 ) # this runs in parallel
        pt_x2_in = pt_x2.set( pt_in_pos_x2 ) # this runs in parallel
        pt_y1_in = pt_y1.set( pt_in_pos_y1 ) # this runs in parallel
        pt_y2_in = pt_y2.set( pt_in_pos_y2 ) # this runs in parallel
        pt_z_in = pt_z.set( pt_in_pos_z ) # this runs in parallel
        for t in (pt_x1_in, pt_x2_in, pt_y1_in, pt_y2_in, pt_z_in): # this waits for all of them to be done!
            t.wait()
    else:
        print( "devises are in the way" )

@as_shortcut
def post_tube_out():

    # safety logic for post-tube out
    if detector_z.get() > 20:
        pt_x1_out = pt_x1.set( pt_out_pos_x1 ) # this runs in parallel
        pt_x2_out = pt_x2.set( pt_out_pos_x2 ) # this runs in parallel
        pt_y1_out = pt_y1.set( pt_out_pos_y1 ) # this runs in parallel
        pt_y2_out = pt_y2.set( pt_out_pos_y2 ) # this runs in parallel
        pt_z_out = pt_z.set( pt_out_pos_z ) # this runs in parallel
        for t in (pt_x1_out, pt_x2_out, pt_y1_out, pt_y2_out, pt_z_out): # this waits for all of them to be done!
            t.wait()
    else:
        print( "detector needs to move" )

@as_shortcut
def coll_in():
    cx_in = coll_x.set( coll_in_pos_x ) # this runs in parallel
    cy_in = coll_y.set( coll_in_pos_y ) # this runs in parallel
    for t in (cx_in, cy_in): # this waits for all of them to be done!
        t.wait()

@as_shortcut
def coll_out():
    cx_out = coll_x.set( coll_out_pos_x ) # this runs in parallel
    cy_out = coll_y.set( coll_out_pos_y ) # this runs in parallel
    for t in (cx_out, cy_out): # this waits for all of them to be done!
        t.wait()

@as_shortcut
def detector_in():

    # safety logic for detector in
    if backlight.get() > 0 and pt_y1.get() > 8:
        detector_z.set( detector_in_pos ).wait() # this runs in parallel
    else:
        print( "devises are in the way" )

@as_shortcut
def detector_out():
    detector_z.set( detector_out_pos ).wait() # this runs in parallel



