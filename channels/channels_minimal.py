# Channels at Cristallina endstation


##########################################################################################################
# BS channels

# TODO: JF settings regarding raw conversion, compression, etc.
detectors_min = [
    # "JF16T03V01",
]


channels_min = ["SARFE10-PSSS059:SPECTRUM_Y"]

pvs_min = ["SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-AVG"]  # pvs_slits + pv_channels + smaract_channels
