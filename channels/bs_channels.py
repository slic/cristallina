# Channels to save at Cristallina endstation

##########################################################################################################
##########################################################################################################
##########################################################################################################
# BS channels

from slic.core.acquisition.detcfg import DetectorConfig

# TODO: JF settings regarding raw conversion, compression, etc.
detectors = [
#    "JF16T03V01",
    "JF17T16V01",
]

# ALLOWED_PARAMS = dict(
#     adc_to_energy = bool,
#     compression = bool,
#     crystfel_lists_laser = bool,
#     disabled_modules = Sequence,
#     double_pixels_action = ["mask", "interp", "keep"],
#     downsample = tuple,
#     factor = Number,
#     geometry = bool,
#     remove_raw_files = bool,
#     roi = dict, #TODO: check on contents of the dict?
#     save_dap_results = bool
# )

detectors_with_config = DetectorConfig(detectors)
# detectors_with_config["JF16T03V01"]['save_dap_results'] = True
# detectors_with_config["JF16T03V01"]['remove_raw_files'] = True
# detectors_with_config["JF16T03V01"]['disabled_modules'] = [0, 1] # bottom module:0, middle module:1, top module:2


detectors_MX = DetectorConfig()
detectors_MX.add("JF17T16V01", adc_to_energy=True, compression=True, crystfel_lists_laser=True, double_pixels_action="mask", factor=12.08, remove_raw_files=True, save_dap_results=True, geometry=False)



camera_channels = [
    # "SARES30-CAMS156-PCO1:FPICTURE",       # PCO edge camera for the wavefront analysis (from Alvra)
#    "SARES30-CAMS156-SMX-OAV:FPICTURE",    # SwissMX OAV camera picture
#    "SARES30-CAMS156-SMX-OAV.jet_projection", #SWISSMX oav jET PROJECTION
    # "SARES30-CAMS156-XE:FPICTURE",         # X-ray eye
]

####################
# Machine gas intensity monitor
channels_gas_monitor = [
    "SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-AVG",
    # "SARFE10-PBPG050:SLOW-X",
    # "SARFE10-PBPG050:SLOW-Y",
    "SARFE10-PBIG050-EVR0:CALCI",  # good for correlations with total beam intensity
    "SARFE10-PBPG050:HAMP-INTENSITY-CAL",
]

# RF phases and amplitudes
channels_RF = [
    "SINSB01-RLLE-DSP:PHASE-VS",
    "SINSB02-RLLE-DSP:PHASE-VS",
    "SINSB03-RLLE-DSP:PHASE-VS",
    "SINSB04-RLLE-DSP:PHASE-VS",
    "SINXB01-RLLE-DSP:PHASE-VS",
    "SINDI01-RLLE-DSP:PHASE-VS",
    "S10CB01-RLLE-DSP:PHASE-VS",
    "S10CB02-RLLE-DSP:PHASE-VS",
    "S10CB03-RLLE-DSP:PHASE-VS",
    "S10CB04-RLLE-DSP:PHASE-VS",
    "S10CB05-RLLE-DSP:PHASE-VS",
    "S10CB06-RLLE-DSP:PHASE-VS",
    "S10CB07-RLLE-DSP:PHASE-VS",
    "S10CB08-RLLE-DSP:PHASE-VS",
    "S10CB09-RLLE-DSP:PHASE-VS",
    "S20CB01-RLLE-DSP:PHASE-VS",
    "S20CB02-RLLE-DSP:PHASE-VS",
    "S20CB03-RLLE-DSP:PHASE-VS",
    "S20CB04-RLLE-DSP:PHASE-VS",
    "S30CB01-RLLE-DSP:PHASE-VS",
    "S30CB02-RLLE-DSP:PHASE-VS",
    "S30CB03-RLLE-DSP:PHASE-VS",
    "S30CB04-RLLE-DSP:PHASE-VS",
    "S30CB05-RLLE-DSP:PHASE-VS",
    "S30CB06-RLLE-DSP:PHASE-VS",
    "S30CB07-RLLE-DSP:PHASE-VS",
    "S30CB08-RLLE-DSP:PHASE-VS",
    "S30CB09-RLLE-DSP:PHASE-VS",
    "S30CB10-RLLE-DSP:PHASE-VS",
    "S30CB11-RLLE-DSP:PHASE-VS",
    "S30CB12-RLLE-DSP:PHASE-VS",
    "S30CB13-RLLE-DSP:PHASE-VS",
    "S30CB14-RLLE-DSP:PHASE-VS",
    "SINEG01-RLLE-DSP:AMPLT-VS",
    "SINSB01-RLLE-DSP:AMPLT-VS",
    "SINSB02-RLLE-DSP:AMPLT-VS",
    "SINSB03-RLLE-DSP:AMPLT-VS",
    "SINSB04-RLLE-DSP:AMPLT-VS",
    "SINXB01-RLLE-DSP:AMPLT-VS",
    "SINDI01-RLLE-DSP:AMPLT-VS",
    "S10CB01-RLLE-DSP:AMPLT-VS",
    "S10CB02-RLLE-DSP:AMPLT-VS",
    "S10CB03-RLLE-DSP:AMPLT-VS",
    "S10CB04-RLLE-DSP:AMPLT-VS",
    "S10CB05-RLLE-DSP:AMPLT-VS",
    "S10CB06-RLLE-DSP:AMPLT-VS",
    "S10CB07-RLLE-DSP:AMPLT-VS",
    "S10CB08-RLLE-DSP:AMPLT-VS",
    "S10CB09-RLLE-DSP:AMPLT-VS",
    "S20CB01-RLLE-DSP:AMPLT-VS",
    "S20CB02-RLLE-DSP:AMPLT-VS",
    "S20CB03-RLLE-DSP:AMPLT-VS",
    "S20CB04-RLLE-DSP:AMPLT-VS",
    "S30CB01-RLLE-DSP:AMPLT-VS",
    "S30CB02-RLLE-DSP:AMPLT-VS",
    "S30CB03-RLLE-DSP:AMPLT-VS",
    "S30CB04-RLLE-DSP:AMPLT-VS",
    "S30CB05-RLLE-DSP:AMPLT-VS",
    "S30CB06-RLLE-DSP:AMPLT-VS",
    "S30CB07-RLLE-DSP:AMPLT-VS",
    "S30CB08-RLLE-DSP:AMPLT-VS",
    "S30CB09-RLLE-DSP:AMPLT-VS",
    "S30CB10-RLLE-DSP:AMPLT-VS",
    "S30CB11-RLLE-DSP:AMPLT-VS",
    "S30CB12-RLLE-DSP:AMPLT-VS",
    "S30CB13-RLLE-DSP:AMPLT-VS",
    "S30CB14-RLLE-DSP:AMPLT-VS",
]

channels_Xeye = ["SARES30-CAMS156-XE:intensity",
                "SARES30-CAMS156-XE:x_center_of_mass",
                "SARES30-CAMS156-XE:x_fit_amplitude",
                "SARES30-CAMS156-XE:x_fit_mean",
                "SARES30-CAMS156-XE:x_fit_offset",
                "SARES30-CAMS156-XE:x_fit_standard_deviation",
                "SARES30-CAMS156-XE:x_fwhm",
                "SARES30-CAMS156-XE:x_profile",
                "SARES30-CAMS156-XE:x_rms",
                "SARES30-CAMS156-XE:y_center_of_mass",
                "SARES30-CAMS156-XE:y_fit_amplitude",
                "SARES30-CAMS156-XE:y_fit_mean",
                "SARES30-CAMS156-XE:y_fit_offset",
                "SARES30-CAMS156-XE:y_fit_standard_deviation",
                "SARES30-CAMS156-XE:y_fwhm",
                "SARES30-CAMS156-XE:y_profile",
                "SARES30-CAMS156-XE:y_rms",
                 ]

######################
# PBPS053
channels_PBPS053 = [
    "SARFE10-PBPS053:INTENSITY",
    "SARFE10-PBPS053:XPOS",
    "SARFE10-PBPS053:YPOS",
]

####################
# PSSS059
channels_PSSS059 = [
    "SARFE10-PSSS059:FIT-COM",
    "SARFE10-PSSS059:FIT-FWHM",
    "SARFE10-PSSS059:FIT-RES",
    "SARFE10-PSSS059:FIT-RMS",
    "SARFE10-PSSS059:SPECT-COM",
    "SARFE10-PSSS059:SPECT-RES",
    "SARFE10-PSSS059:SPECT-RMS",
    "SARFE10-PSSS059:SPECTRUM_Y_SUM",
    "SARFE10-PSSS059:SPECTRUM_X",
    "SARFE10-PSSS059:SPECTRUM_Y",
    "SARFE10-PSSS059:FPICTURE",
    "SARFE10-PSSS059:processing_parameters",
]

# Large bandwidth camera
channels_PSSS059_LB = [
    "SARFE10-PSSS059-LB:FIT-COM",
    "SARFE10-PSSS059-LB:FIT-FWHM",
    "SARFE10-PSSS059-LB:FIT-RES",
    "SARFE10-PSSS059-LB:FIT-RMS",
    "SARFE10-PSSS059-LB:SPECT-COM",
    "SARFE10-PSSS059-LB:SPECT-RES",
    "SARFE10-PSSS059-LB:SPECT-RMS",
    "SARFE10-PSSS059-LB:SPECTRUM_X",
    "SARFE10-PSSS059-LB:SPECTRUM_Y",
    "SARFE10-PSSS059-LB:SPECTRUM_Y_SUM",
    "SARFE10-PSSS059-LB:processing_parameters",
    "SARFE10-PSSS059-LB:FPICTURE",
    "SARFE10-PSSS059-LB:FIT-BRT",
]

###################################
## Bernina channels
# Beam position monitor PBPS113
channels_Bernina = [
    "SAROP21-PBPS103:INTENSITY",
    "SAROP21-PBPS103:XPOS",
    "SAROP21-PBPS103:YPOS",
    #"SAROP21-PPRM113:FPICTURE",
    "SAROP21-PPRM113:intensity",
    "SAROP21-PPRM113:x_fit_mean",
    "SAROP21-PPRM113:y_fit_mean",
]
###################################
# Beam position monitor PBPS113
channels_PBPS113 = [
    "SAROP31-PBPS113:INTENSITY",
    "SAROP31-PBPS113:INTENSITY_UJ",
    "SAROP31-PBPS113:Lnk9Ch0-PP_VAL_PD0",
    "SAROP31-PBPS113:Lnk9Ch0-PP_VAL_PD1",
    "SAROP31-PBPS113:Lnk9Ch0-PP_VAL_PD2",
    "SAROP31-PBPS113:Lnk9Ch0-PP_VAL_PD3",
    "SAROP31-PBPS113:XPOS",
    "SAROP31-PBPS113:YPOS",
]
channels_PBPS113_waveforms = [
    "SAROP31-PBPS113:Lnk9Ch0-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch1-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch2-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch3-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch4-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch5-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch6-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch7-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch8-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch9-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch10-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch11-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch12-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch13-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch14-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch15-WF-DATA",
]

####################
# Profile monitor PPRM113 (from _proc process)
channels_PPRM113 = [
    "SAROP31-PPRM113:intensity",
    "SAROP31-PPRM113:x_center_of_mass",
    "SAROP31-PPRM113:x_fit_amplitude",
    "SAROP31-PPRM113:x_fit_mean",
    "SAROP31-PPRM113:x_fit_offset",
    "SAROP31-PPRM113:x_fit_standard_deviation",
    "SAROP31-PPRM113:x_fwhm",
    "SAROP31-PPRM113:x_profile",
    "SAROP31-PPRM113:x_rms",
    "SAROP31-PPRM113:y_center_of_mass",
    "SAROP31-PPRM113:y_fit_amplitude",
    "SAROP31-PPRM113:y_fit_mean",
    "SAROP31-PPRM113:y_fit_offset",
    "SAROP31-PPRM113:y_fit_standard_deviation",
    "SAROP31-PPRM113:y_fwhm",
    "SAROP31-PPRM113:y_profile",
    "SAROP31-PPRM113:y_rms",
    # "SAROP31-PPRM113:FPICTURE",                         # full pictures for debugging purposes at the moment, from _ib process
]

###########################
# Beam position monitor PBPS149
# "SARES30-CAMS156-PCO1:FPICTURE",       # PCO edge camera for the wavefront analysis (from Alvra)
# "SARES30-CAMS156-SMX-OAV:FPIC

###########################
# Beam position monitor
channels_PBPS149 = [
    "SAROP31-PBPS149:INTENSITY",
    "SAROP31-PBPS149:INTENSITY_UJ",
    "SAROP31-PBPS149:Lnk9Ch0-PP_VAL_PD0",
    "SAROP31-PBPS149:Lnk9Ch0-PP_VAL_PD1",
    "SAROP31-PBPS149:Lnk9Ch0-PP_VAL_PD2",
    "SAROP31-PBPS149:Lnk9Ch0-PP_VAL_PD3",
    "SAROP31-PBPS149:XPOS",
    "SAROP31-PBPS149:YPOS",
]

#######################
# Profile monitor PPRM150 (from _proc process)
channels_PPRM150 = [
    "SAROP31-PPRM150:intensity",
    "SAROP31-PPRM150:x_center_of_mass",
    "SAROP31-PPRM150:x_fit_amplitude",
    "SAROP31-PPRM150:x_fit_mean",
    "SAROP31-PPRM150:x_fit_offset",
    "SAROP31-PPRM150:x_fit_standard_deviation",
    "SAROP31-PPRM150:x_fwhm",
    "SAROP31-PPRM150:x_profile",
    "SAROP31-PPRM150:x_rms",
    "SAROP31-PPRM150:y_center_of_mass",
    "SAROP31-PPRM150:y_fit_amplitude",
    "SAROP31-PPRM150:y_fit_mean",
    "SAROP31-PPRM150:y_fit_offset",
    "SAROP31-PPRM150:y_fit_standard_deviation",
    "SAROP31-PPRM150:y_fwhm",
    "SAROP31-PPRM150:y_profile",
    "SAROP31-PPRM150:y_rms",
    # "SAROP31-PPRM150:FPICTURE",                         # full pictures for debugging purposes at the moment, from _ib process
]

#######################
# Cristallina event reciever
channels_EVR = [
    "SAR-CVME-TIFALL6:EvtSet",
]

#######################
# Digitizer
channels_digitizer = [
     # extra non-beam synchronous channels:
    #"SARES30-LTIM01-EVR0:DUMMY_PV1_NBS",
    #"SARES30-LTIM01-EVR0:DUMMY_PV2_NBS",
    #"SARES30-LTIM01-EVR0:DUMMY_PV3_NBS",
    #"SARES30-LTIM01-EVR0:DUMMY_PV4_NBS",
    # other EVR channels:
    "SARES30-LSCP1-FNS:CH0:VAL_GET",  # Signal-Background
    "SARES30-LSCP1-CRISTA1:CH0:1",  # Waveform signal
    "SARES30-LSCP1-CRISTA1:CH2:1",  # Waveform trigger
    "SARES30-LTIM01-EVR0:CALCI",  # Calculated intensity
]

#######################
# Other BS channels that we sometimes use
channels_other = [
        # "SARFE10-PPRM053:FPICTURE", # TODO: Test if this works here
        # "SARFE10-PPRM064:FPICTURE", # TODO: Test if this works here
        ]

bs_channels = (
    camera_channels
    + channels_gas_monitor
    # + channels_RF
    # + channels_Xeye
    + channels_PBPS053
    + channels_PSSS059
    + channels_PBPS113
    # + channels_PBPS113_waveforms
    # + channels_PPRM113
    + channels_PBPS149
    # + channels_PBPS149_waveforms
    # + channels_PPRM150 # only if screen is inserted
    + channels_EVR
    # + channels_digitizer
    + channels_other
)

