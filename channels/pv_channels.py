##########################################################################################################
##########################################################################################################
##########################################################################################################
# Epics PVS

# Compression, charge settings
#######################
# Machine
pvs_machine = [
    "SARCL02-MBND100:P-READ",  # Predicted bunch energy
    "SARUN:FELPHOTENE.VAL",    # Predicted photon energy from machine settings
    "SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-AVG.VAL",  # Average pulse energy from the gas detector
]

# accelerator parameters
pvs_RF = [
    "SINSB01-RSYS:GET-VSUM-PHASE-OFFSET",
    "SINSB02-RSYS:GET-VSUM-PHASE-OFFSET",
    "SINSB03-RSYS:GET-VSUM-PHASE-OFFSET",
    "SINSB04-RSYS:GET-VSUM-PHASE-OFFSET",
    "SINXB01-RSYS:GET-VSUM-PHASE-OFFSET",
    "SINDI01-RSYS:GET-VSUM-PHASE-OFFSET",
    "S10CB01-RSYS:GET-VSUM-PHASE-OFFSET",
    "S10CB02-RSYS:GET-VSUM-PHASE-OFFSET",
    "S10CB03-RSYS:GET-VSUM-PHASE-OFFSET",
    "S10CB04-RSYS:GET-VSUM-PHASE-OFFSET",
    "S10CB06-RSYS:GET-VSUM-PHASE-OFFSET",
    "S10CB05-RSYS:GET-VSUM-PHASE-OFFSET",
    "S10CB07-RSYS:GET-VSUM-PHASE-OFFSET",
    "S10CB08-RSYS:GET-VSUM-PHASE-OFFSET",
    "S10CB09-RSYS:GET-VSUM-PHASE-OFFSET",
    "S20CB01-RSYS:GET-VSUM-PHASE-OFFSET",
    "S20CB02-RSYS:GET-VSUM-PHASE-OFFSET",
    "S20CB03-RSYS:GET-VSUM-PHASE-OFFSET",
    "S20CB04-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB01-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB02-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB03-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB04-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB05-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB06-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB07-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB08-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB09-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB10-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB11-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB12-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB13-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB14-RSYS:GET-VSUM-PHASE-OFFSET",
    "SINEG01-RSYS:GET-VSUM-AMPLT-SCALE",
    "SINSB01-RSYS:GET-VSUM-AMPLT-SCALE",
    "SINSB02-RSYS:GET-VSUM-AMPLT-SCALE",
    "SINSB03-RSYS:GET-VSUM-AMPLT-SCALE",
    "SINSB04-RSYS:GET-VSUM-AMPLT-SCALE",
    "SINXB01-RSYS:GET-VSUM-AMPLT-SCALE",
    "SINDI01-RSYS:GET-VSUM-AMPLT-SCALE",
    "S10CB01-RSYS:GET-VSUM-AMPLT-SCALE",
    "S10CB02-RSYS:GET-VSUM-AMPLT-SCALE",
    "S10CB03-RSYS:GET-VSUM-AMPLT-SCALE",
    "S10CB04-RSYS:GET-VSUM-AMPLT-SCALE",
    "S10CB05-RSYS:GET-VSUM-AMPLT-SCALE",
    "S10CB06-RSYS:GET-VSUM-AMPLT-SCALE",
    "S10CB07-RSYS:GET-VSUM-AMPLT-SCALE",
    "S10CB08-RSYS:GET-VSUM-AMPLT-SCALE",
    "S10CB09-RSYS:GET-VSUM-AMPLT-SCALE",
    "S20CB01-RSYS:GET-VSUM-AMPLT-SCALE",
    "S20CB02-RSYS:GET-VSUM-AMPLT-SCALE",
    "S20CB03-RSYS:GET-VSUM-AMPLT-SCALE",
    "S20CB04-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB01-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB02-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB03-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB04-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB05-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB06-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB07-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB08-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB09-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB10-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB11-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB12-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB13-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB14-RSYS:GET-VSUM-AMPLT-SCALE",
]

#######################
# Undulator gap
pvs_undulator = [
    "SARUN03-UIND030:K_SET.VAL",
    "SARUN04-UIND030:K_SET.VAL",
    "SARUN05-UIND030:K_SET.VAL",
    "SARUN06-UIND030:K_SET.VAL",
    "SARUN07-UIND030:K_SET.VAL",
    "SARUN08-UIND030:K_SET.VAL",
    "SARUN09-UIND030:K_SET.VAL",
    "SARUN10-UIND030:K_SET.VAL",
    "SARUN11-UIND030:K_SET.VAL",
    "SARUN12-UIND030:K_SET.VAL",
    "SARUN13-UIND030:K_SET.VAL",
    "SARUN14-UIND030:K_SET.VAL",
    "SARUN15-UIND030:K_SET.VAL",
]

####################
# Machine gas intensity monitor
pvs_gas_monitor = [
    "SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-US",
    "SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-DS",
]

#####################
# Slits OAPU044
pvs_OAPU044 = [
    "SARFE10-OAPU044:MOTOR_X",
    "SARFE10-OAPU044:MOTOR_Y",
    "SARFE10-OAPU044:MOTOR_W",
    "SARFE10-OAPU044:MOTOR_H",
]

###################
# Beam position monitor PBPS053
pvs_PBPS053 = [
    "SARFE10-PBPS053:MOTOR_X1",
    "SARFE10-PBPS053:MOTOR_X2",
    "SARFE10-PBPS053:MOTOR_PROBE",
]

###################
# Spectrometer PSSS059
pvs_PSSS059 = [
    "SARFE10-PSSS055:MOTOR_X1.RBV",
    "SARFE10-PSSS055:MOTOR_Y1.RBV",
    "SARFE10-PSSS055:MOTOR_ROT_X1.RBV",
    "SARFE10-PSSS055:MOTOR_PROBE.RBV",
    "SARFE10-PSSS059:MOTOR_X3.RBV",
    "SARFE10-PSSS059:MOTOR_Y3.RBV",
    "SARFE10-PSSS059:MOTOR_ROT_X3.RBV",
    "SARFE10-PSSS059:MOTOR_Y4.RBV",
    "SARFE10-PSSS059:MOTOR_ROT_X4.RBV",
    "SARFE10-PSSS059:MOTOR_X5.RBV",
    "SARFE10-PSSS059:MOTOR_Y5.RBV",
    "SARFE10-PSSS059:MOTOR_Z5.RBV",
    "SARFE10-PSSS055:GRATING_SP",
    "SARFE10-PSSS059:CRYSTAL_SP",
    "SARFE10-PSSS059:SPC_ROI_YMIN",
    "SARFE10-PSSS059:SPC_ROI_YMAX",
]

####################
# Upstream attenuator OATT053
pvs_OATT053_old = [
    "SARFE10-OATT053:MOTOR_1",
    "SARFE10-OATT053:MOTOR_1.RBV",
    "SARFE10-OATT053:MOTOR_2",
    "SARFE10-OATT053:MOTOR_2.RBV",
    "SARFE10-OATT053:MOTOR_3",
    "SARFE10-OATT053:MOTOR_3.RBV",
    "SARFE10-OATT053:MOTOR_4",
    "SARFE10-OATT053:MOTOR_4.RBV",
    "SARFE10-OATT053:MOTOR_5",
    "SARFE10-OATT053:MOTOR_5.RBV",
    "SARFE10-OATT053:MOTOR_6",
    "SARFE10-OATT053:MOTOR_6.RBV",
    "SARFE10-OATT053:ENERGY",
    "SARFE10-OATT053:TRANS_SP",
    "SARFE10-OATT053:TRANS_RB",
]


####################
# New Upstream attenuator OATT053
pvs_OATT053 = [
    "SARFE10-OATT053:photonenergy", #        Photon energy for Transmission          
    "SARFE10-OATT053:transmission", #        Total Transmission of all stages        
    "SARFE10-OATT053:transmission3", #
    "SARFE10-OATT053:PHOTONENERGY_local", # Manually entered Photon Energy 
    "SARFE10-OATT053:ENERGY", #  Energy eV"
    "SARFE10-OATT053:MOTOR_1",  #                  motor             Motor 1         
    "SARFE10-OATT053:MOTOR_2",  #                   motor             Motor 2        
    "SARFE10-OATT053:MOTOR_3",  #                   motor             Motor 3        
    "SARFE10-OATT053:MOTOR_4",  #                   motor             Motor 4        
    "SARFE10-OATT053:MOTOR_5",  #                   motor             Motor 5        
    "SARFE10-OATT053:MOTOR_6",  #                   motor             Motor 6  
]

###################
# Beam profile monitor PPRM053
pvs_PPRM053 = [
    "SARFE10-PPRM053:MOTOR_PROBE.RBV",
    #"SARFE10-PPRM053:FPICTURE", 
    #"SARFE10-PPRM064:FPICTURE", # TODO move to correct place
]

###################
# Bernina mono
pvs_Bernina = [
    "SAROP21-ARAMIS:ENERGY_SP",
    "SAROP21-ARAMIS:ENERGY",
    "SAROP21-PBPS103:MOTOR_X1.DRBV",
    "SAROP21-PBPS103:MOTOR_Y1.DRBV",
    "SAROP21-PBPS103:MOTOR_X1.RBV",
    "SAROP21-PBPS103:MOTOR_Y1.RBV",
    "SAROP21-PBPS103:MOTOR_PROBE.RBV",
    "SAROP21-PPRM113:MOTOR_PROBE.RBV"
]


####################
# First Cristallina horizontal offset mirror OOMH067
pvs_OOMH067 = [
    "SAROP31-OOMH067:W_X.RBV",
    "SAROP31-OOMH067:W_Y.RBV",
    "SAROP31-OOMH067:W_RX.RBV",
    "SAROP31-OOMH067:W_RY.RBV",
    "SAROP31-OOMH067:W_RZ.RBV",
    "SAROP31-OOMH067:BU.RBV",
    "SAROP31-OOMH067:BD.RBV",
    "SAROP31-OOMH067:VS1.RBV",
    "SAROP31-OOMH067:VS2.RBV",
    "SAROP31-OOMH067:VS3.RBV",
    "SAROP31-OOMH067:TX.RBV",
    "SAROP31-OOMH067:RY.RBV",
]

####################
# Beam screen between the first two horizontal mirrors PSCR068
pvs_PSCR068 = [
    "SAROP31-PSCR068:MOTOR_PROBE.RBV",
]

####################
# Second Cristallina horizontal offset mirror OOMH084
pvs_OOMH084 = [
    "SAROP31-OOMH084:W_X.RBV",
    "SAROP31-OOMH084:W_Y.RBV",
    "SAROP31-OOMH084:W_RX.RBV",
    "SAROP31-OOMH084:W_RY.RBV",
    "SAROP31-OOMH084:W_RZ.RBV",
    "SAROP31-OOMH084:BU.RBV",
    "SAROP31-OOMH084:BD.RBV",
    "SAROP31-OOMH084:VS1.RBV",
    "SAROP31-OOMH084:VS2.RBV",
    "SAROP31-OOMH084:VS3.RBV",
    "SAROP31-OOMH084:TX.RBV",
    "SAROP31-OOMH084:RY.RBV",
]

###################
# Beam profile monitor PPRM085
pvs_PPRM085 = [
    "SAROP31-PPRM085:MOTOR_PROBE.RBV",
]

###################
# Slits OAPU107
pvs_OAPU107 = [
    "SAROP31-OAPU107:MOTOR_X.VAL",
    "SAROP31-OAPU107:MOTOR_X.RBV",
    "SAROP31-OAPU107:MOTOR_Y.VAL",
    "SAROP31-OAPU107:MOTOR_Y.RBV",
]

###################
## Beam position and intensity monitor PBPS113
pvs_PBPS113 = [
    "SAROP31-PBPS113:MOTOR_X1.DRBV",
    "SAROP31-PBPS113:MOTOR_Y1.DRBV",
    "SAROP31-PBPS113:MOTOR_X1.RBV",
    "SAROP31-PBPS113:MOTOR_Y1.RBV",
    "SAROP31-PBPS113:MOTOR_PROBE.RBV",
]

###################
# Beam profile monitor PPRM113
pvs_PPRM113 = [
    "SAROP31-PPRM113:MOTOR_PROBE.RBV",
]

####################
# Alignment laser mirror OLAS147
pvs_OLAS147 = [
    "SAROP31-OLAS147:MOTOR_1.RBV",
]

###################
# Slits OAPU149
pvs_OAPU149 = [
    "SAROP31-OAPU149:MOTOR_X.RBV",
    "SAROP31-OAPU149:MOTOR_Y.RBV",
    "SAROP31-OAPU149:MOTOR_W.RBV",
    "SAROP31-OAPU149:MOTOR_H.RBV",
]

###################
# Beam position and intensity monitor PBPS149
pvs_PBPS149 = [
    "SAROP31-PBPS149:MOTOR_X1.DRBV",
    "SAROP31-PBPS149:MOTOR_Y1.DRBV",
    "SAROP31-PBPS149:MOTOR_X1.RBV",
    "SAROP31-PBPS149:MOTOR_Y1.RBV",
    "SAROP31-PBPS149:MOTOR_PROBE.RBV",
]

###################
# Beam profile monitor PPRM150
pvs_PPRM150 = [
    "SAROP31-PPRM150:MOTOR_PROBE.RBV",
]

####################
# Attenuators OATA150
pvs_OATA150_old = [
    "SAROP31-OATA150:MOTOR_1.RBV",
    "SAROP31-OATA150:MOTOR_2.RBV",
    "SAROP31-OATA150:MOTOR_3.RBV",
    "SAROP31-OATA150:MOTOR_4.RBV",
    "SAROP31-OATA150:MOTOR_5.RBV",
    "SAROP31-OATA150:MOTOR_6.RBV",
    "SAROP31-OATA150:ENERGY",
    "SAROP31-OATA150:TRANS_SP",
    "SAROP31-OATA150:TRANS_RB",
    "SAROP31-OATA150:TRANS3EDHARM_RB",
    "SAROP31-OATA150:MOT2TRANS.VALD"
]

####################
# New Attenuators OATA150
pvs_OATA150 = [
    "SAROP31-OATA150:photonenergy", #              ai                Photon energy for Transmission            SAROP31-CPCL-OSAT150       swissfel
    "SAROP31-OATA150:transmission", #         ai                Total Transmission of all stages          SAROP31-CPCL-OSAT150       swissfel
    "SAROP31-OATA150:transmission3", #
    "SAROP31-OATA150:PHOTONENERGY_local", # Manually entered Photon Energy 
    "SAROP31-OATA150:ENERGY", #  Energy eV"
    "SAROP31-OATA150:MOTOR_1",  #                  motor             Motor 1                                   SAROP31-CPPM-MOT7171       swissfel
    "SAROP31-OATA150:MOTOR_2",  #                   motor             Motor 2                                   SAROP31-CPPM-MOT7171       swissfel
    "SAROP31-OATA150:MOTOR_3",  #                   motor             Motor 3                                   SAROP31-CPPM-MOT7171       swissfel
    "SAROP31-OATA150:MOTOR_4",  #                   motor             Motor 4                                   SAROP31-CPPM-MOT7171       swissfel
    "SAROP31-OATA150:MOTOR_5",  #                   motor             Motor 5                                   SAROP31-CPPM-MOT7171       swissfel
    "SAROP31-OATA150:MOTOR_6",  #                   motor             Motor 6  
]



####################
# Pulse picker OPPI151
pvs_OPPI151 = [
    "SAROP31-OPPI151:MOTOR_X.RBV",
    "SAROP31-OPPI151:MOTOR_Y.RBV",
]


####################
## Horizontal offset mirror ODMV152
pvs_ODMV152 = [
    "SAROP31-ODMV152:W_X.RBV",
    "SAROP31-ODMV152:W_Y.RBV",
    "SAROP31-ODMV152:W_RX.RBV",
    "SAROP31-ODMV152:W_RZ.RBV",
    "SAROP31-ODMV152:BU.RBV",
    "SAROP31-ODMV152:BD.RBV",
    "SAROP31-ODMV152:VS1.RBV",
    "SAROP31-ODMV152:VS2.RBV",
    "SAROP31-ODMV152:VS3.RBV",
    "SAROP31-ODMV152:TX.RBV",
]

###########################
# Vertical KB mirror OKBV153
pvs_OKBV153 = [
    "SAROP31-OKBV153:W_X.RBV",
    "SAROP31-OKBV153:W_Y.RBV",
    "SAROP31-OKBV153:W_RX.RBV",
    "SAROP31-OKBV153:W_RY.RBV",
    "SAROP31-OKBV153:W_RZ.RBV",
    "SAROP31-OKBV153:BU.RBV",
    "SAROP31-OKBV153:BD.RBV",
    "SAROP31-OKBV153:TY1.RBV",
    "SAROP31-OKBV153:TY2.RBV",
    "SAROP31-OKBV153:TY3.RBV",
    "SAROP31-OKBV153:TX1.RBV",
    "SAROP31-OKBV153:TX2.RBV",
]

####################
# Screen between the KB's PSCD153
pvs_PSCD153 = [
    "SAROP31-PSCD153:MOTOR_PROBE.RBV"
]

###########################
# Horizontal KB mirror OKBH154
pvs_OKBH154 = [
    "SAROP31-OKBH154:W_X.RBV",
    "SAROP31-OKBH154:W_Y.RBV",
    "SAROP31-OKBH154:W_RX.RBV",
    "SAROP31-OKBH154:W_RY.RBV",
    "SAROP31-OKBH154:W_RZ.RBV",
    "SAROP31-OKBH154:BU.RBV",
    "SAROP31-OKBH154:BD.RBV",
    "SAROP31-OKBH154:TY1.RBV",
    "SAROP31-OKBH154:TY2.RBV",
    "SAROP31-OKBH154:TY3.RBV",
    "SAROP31-OKBH154:TX2.RBV",
]

####################
# Standa motors (mainly used with the X-ray eye)
pvs_standa = [
    "SARES30-MOBI1:MOT_1.RBV",
    "SARES30-MOBI1:MOT_2.RBV",
    "SARES30-MOBI1:MOT_3.RBV",
]

####################
# Newport 300 mm stage
pvs_newport_300 = [
    "SARES30-MOBI1:MOT_5.RBV",
]


###############################
# Smaract stages mini XYZ from SwissMX
pvs_smaract_xyz = [
    "SARES30-MCS2750:MOT_1.RBV",
    "SARES30-MCS2750:MOT_1.VAL",
    "SARES30-MCS2750:MOT_2.RBV",
    "SARES30-MCS2750:MOT_2.VAL",
    "SARES30-MCS2750:MOT_3.RBV",
    "SARES30-MCS2750:MOT_3.VAL",
]

####################
# Attocube motors
pvs_attocube = [
    "SARES30-ATTOCUBE:A0-POS",
    "SARES30-ATTOCUBE:A1-POS",
]

###############################
# Smaract stages from Juraj
pvs_smaract_juraj = [
    "SARES30-XSMA156:X:MOTRBV",
    "SARES30-XSMA156:Y:MOTRBV",
    "SARES30-XSMA156:Z:MOTRBV",
    "SARES30-XSMA156:Ry:MOTRBV",
    "SARES30-XSMA156:Rx:MOTRBV",
    "SARES30-XSMA156:Rz:MOTRBV",
]

pvs_diffractometer_1 = [
    "SARES30-CPCL-ECMC02:ROT2THETA-PosAct",
    "SARES30-CPCL-ECMC02:ROTTHETA-PosAct",
    "SARES30-CPCL-ECMC02:TRXBASE-PosAct",
    "SARES30-CPCL-ECMC02:TRY-PosAct",
    "SARES30-CPCL-ECMC02:TRX-PosAct",
    "SARES30-CPCL-ECMC02:TRZ-PosAct",
    "SARES30-CPCL-ECMC02:TD-PosAct",
]

pvs = (
    pvs_machine
    # + pvs_RF
    # + pvs_undulator
    + pvs_gas_monitor
    + pvs_OAPU044
    + pvs_PBPS053
    + pvs_OATT053
    + pvs_PPRM053
    + pvs_PSSS059
    + pvs_OOMH067
    + pvs_PSCR068
    + pvs_OOMH084
    + pvs_PPRM085
    + pvs_OAPU107
    + pvs_PBPS113
    + pvs_PPRM113
    + pvs_OLAS147
    + pvs_OAPU149
    + pvs_PBPS149
    + pvs_PPRM150
    + pvs_OATA150
    + pvs_OPPI151
    + pvs_ODMV152
    + pvs_OKBV153
    + pvs_PSCD153
    + pvs_OKBH154
    + pvs_standa
#   + pvs_newport_300
    # + pvs_smaract_xyz
    + pvs_diffractometer_1
#    + pvs_Bernina
)


# + pvs_attocube
# + pvs_smaract_juraj
