from slic.core.adjustable import PVAdjustable, PVEnumAdjustable
from slic.core.device.simpledevice import SimpleDevice
from slic.utils import as_shortcut


class PVStringAdjustable(PVAdjustable):
    def get_current_value(self):
        return str(self.pvs.readback.get(as_string=True)).strip()


n_unds = [6, 7, 8, 9, 10, 11, 12, 13, 15, 16, 17, 18, 19, 20, 21, 22]  # 14 is the CHIC

# UND_NAME_FMT = "SARUN{:02}-UIND030"
# N_UND_CHIC = None

# N_UNDS = list(range(3, 15+1))


# undulator_info = {}
# for i in n_unds:
#    undulator_info[f"energy{i}"]       = PVAdjustable(f"SARUN{i:02}-UIND030:FELPHOTENE",  internal=True)
#    undulator_info[f"polarisation{i}"] = PVEnumAdjustable(f"SARUN{i:02}-UIND030:POL-SET", internal=True)


overview = SimpleDevice(
    "Cristallina Overview",
    # OKHB154_X_trans       = PVAdjustable("SAROP31-OKBH154:W_X.RBV", internal=True),
    # OKHB154_Y_trans       = PVAdjustable("SAROP31-OKBH154:W_Y.RBV", internal=True),
    # OKHB154_X_rot         = PVAdjustable("SAROP31-OKBH154:W_RX.RBV", internal=True),
    # OKHB154_Y_rot         = PVAdjustable("SAROP31-OKBH154:W_RY.RBV", internal=True),
    # OKHB154_Z_rot         = PVAdjustable("SAROP31-OKBH154:W_RZ.RBV", internal=True),
    # OKBV153_X_trans       = PVAdjustable("SAROP31-OKBV153:W_X.RBV", internal=True),
    # OKBV153_Y_trans       = PVAdjustable("SAROP31-OKBV153:W_Y.RBV", internal=True),
    # OKBV153_X_rot         = PVAdjustable("SAROP31-OKBV153:W_RX.RBV", internal=True),
    # OKBV153_Y_rot         = PVAdjustable("SAROP31-OKBV153:W_RY.RBV", internal=True),
    # OKBV153_Z_rot         = PVAdjustable("SAROP31-OKBV153:W_RZ.RBV", internal=True),
    # ODMV152_X_trans       = PVAdjustable("SAROP31-OKBV153:W_X.RBV", internal=True),
    # ODMV152_Y_trans       = PVAdjustable("SAROP31-OKBV153:W_Y.RBV", internal=True),
    # ODMV152_X_rot         = PVAdjustable("SAROP31-OKBV153:W_RX.RBV", internal=True),
    # ODMV152_Y_rot         = PVAdjustable("SAROP31-OKBV153:W_RY.RBV", internal=True),
    # ODMV152_COATING       = PVAdjustable("SAROP31-ODMV152:COATING_SP", internal=True),
    OATA150_transmission=PVAdjustable("SAROP31-OATA150:TRANS_SP", internal=True),
    # PBPS149_motor_probe   = PVAdjustable("SAROP31-PBPS149:MOTOR_PROBE.RBV", internal=True),
    # PBPS149_motor_X1      = PVAdjustable("SAROP31-PBPS149:MOTOR_X1.RBV", internal=True),
    # PBPS149_motor_Y1      = PVAdjustable("SAROP31-PBPS149:MOTOR_Y1.RBV", internal=True),
    # PBPS149_wafer         = PVAdjustable("SAROP31-PBPS149:PROBE_SP", internal=True),
    # OAPU149_hor_pos       = PVAdjustable("SAROP31-OAPU149:MOTOR_X.VAL", internal=True),
    # OAPU149_hor_width     = PVAdjustable("SAROP31-OAPU149:MOTOR_W.VAL", internal=True),
    # OAPU149_vert_pos      = PVAdjustable("SAROP31-OAPU149:MOTOR_Y.VAL", internal=True),
    # OAPU149_vert_width    = PVAdjustable("SAROP31-OAPU149:MOTOR_H.VAL", internal=True),
    # OAPU107_hor_pos       = PVAdjustable("SAROP31-OAPU107:MOTOR_X.VAL", internal=True),
    # OAPU107_hor_width     = PVAdjustable("SAROP31-OAPU107:MOTOR_W.VAL", internal=True),
    # OAPU107_vert_pos      = PVAdjustable("SAROP31-OAPU107:MOTOR_Y.VAL", internal=True),
    # OAPU107_vert_width    = PVAdjustable("SAROP31-OAPU107:MOTOR_H.VAL", internal=True),
    # OOMH084_X_trans       = PVAdjustable("SAROP31-OOMH084:W_X.RBV", internal=True),
    # OOMH084_Y_trans       = PVAdjustable("SAROP31-OOMH084:W_Y.RBV", internal=True),
    # OOMH084_X_rot         = PVAdjustable("SAROP31-OOMH084:W_RX.RBV", internal=True),
    # OOMH084_Y_rot         = PVAdjustable("SAROP31-OOMH084:W_RY.RBV", internal=True),
    # OOMH084_Z_rot         = PVAdjustable("SAROP31-OOMH084:W_RZ.RBV", internal=True),
    # OOMH084_COATING       = PVAdjustable("SAROP31-OOMH084:COATING_SP", internal=True),
    # OOMH067_X_trans       = PVAdjustable("SAROP31-OOMH067:W_X.RBV", internal=True),
    # OOMH067_Y_trans       = PVAdjustable("SAROP31-OOMH067:W_Y.RBV", internal=True),
    # OOMH067_X_rot         = PVAdjustable("SAROP31-OOMH067:W_RX.RBV", internal=True),
    # OOMH067_Y_rot         = PVAdjustable("SAROP31-OOMH067:W_RY.RBV", internal=True),
    # OOMH067_Z_rot         = PVAdjustable("SAROP31-OOMH067:W_RZ.RBV", internal=True),
    # OOMH067_COATING       = PVAdjustable("SAROP31-OOMH067:COATING_SP", internal=True),
    SMARACT_MINI_X=PVAdjustable("SARES30-MCS2750:MOT_2.RBV", internal=True),
    SMARACT_MINI_Y=PVAdjustable("SARES30-MCS2750:MOT_3.RBV", internal=True),
    SMARACT_MINI_Z=PVAdjustable("SARES30-MCS2750:MOT_1.RBV", internal=True),
)


spreadsheet_line = []


@as_shortcut
def print_overview():
    print(overview)


@as_shortcut
def print_line_for_spreadsheet():
    ov = overview.__dict__

    def get(i):
        if i in ov:
            return str(ov[i].get())
        return ""

    res = [get(i) for i in spreadsheet_line]
    res = ",".join(res)
    print(res)
