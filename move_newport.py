# move Newport in steps
import time
import numpy as np

from cristallina import newport
print(newport)

# temporarily disable limits
newport.z.set_epics_limits(-300, 300)

# directions: minus is towards 25 pin connector

# newport.z.mv(-140).wait()
newport.z.reset_current_value_to(0)

# 305 mm travel

x_steps = np.arange(1, 310, 10)

for x in x_steps:
    print(f"at: {x}")
    newport.z.mv(x).wait()
    time.sleep(3)



