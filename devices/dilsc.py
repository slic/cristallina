""" DilSc prototype

"""

from slic.core.adjustable import Adjustable

from slic.core.device import Device, SimpleDevice

from frappy.client import SecopClient
from frappy import states
from frappy.datatypes import StatusType


class Dilution(Device):
    def __init__(self, **kwargs):

        self.name = 'DilSc'
        ID = self.name
        super().__init__(ID, **kwargs)

        self.address = 'dilsc.psi.ch:5000'
        self.dilsc = SecopClient(self.address)
        self.dilsc.connect()

        self.x = MagnetCoil("X", self.dilsc, 'x', limit_low=-0.6, limit_high=0.6)
        self.y = MagnetCoil("Y", self.dilsc, 'y', limit_low=-0.6, limit_high=0.6)
        self.z = MagnetCoil("Z", self.dilsc, 'z', limit_low=-5.2, limit_high=5.2)
        self.T_plato = Thermometer('T_plato', self.dilsc, limit_low=0, limit_high=300)
        self.T_pucksensor = Thermometer('T_pucksensor', self.dilsc, limit_low=0, limit_high=300)


class Thermometer(Adjustable):

    def __init__(self, name, dilsc_connection, limit_low=-0.0001, limit_high=0.0001):
        
        super().__init__(name, limit_low=limit_low, limit_high=limit_high)
        self.dilsc = dilsc_connection
        
            
    def _check_connection(func):
        def checker(self, *args, **kwargs):
            if not self.dilsc.online:
                raise ConnectionError(f'No connection to dilsc at {self.address}')
            else:
                return func(self, *args, **kwargs)
        return checker

    @_check_connection
    def get_current_value(self):
        cacheitem = self.dilsc.getParameter(f'{self.name}', 'value', trycache=False)
        return cacheitem.value
        
    
    @_check_connection
    def set_target_value(self, value):
        self.dilsc.setParameter(f'{self.name}', 'target', value)
            
    @_check_connection
    def is_moving(self):
        response = self.dilsc.getParameter(f'{self.name}','status', trycache=False)
        return response[0][0] > StatusType.PREPARED



class MagnetCoil(Adjustable):

    def __init__(self, name, dilsc_connection, direction, limit_low=-0.0001, limit_high=0.0001):
        
        super().__init__(name, limit_low=-0.0001, limit_high=0.0001)
        
        self.direction = direction.lower()    

        if self.direction not in ["x", "y", "z"]:
            raise ValueError("Direction must be either x, y or z.")
        
        self.dilsc = dilsc_connection
        
            
    def _check_connection(func):
        def checker(self, *args, **kwargs):
            if not self.dilsc.online:
                raise ConnectionError(f'No connection to dilsc at {self.address}')
            else:
                return func(self, *args, **kwargs)
        return checker

    @_check_connection
    def get_current_value(self):
        cacheitem = self.dilsc.getParameter(f'mf{self.direction}', 'value', trycache=False)
        return cacheitem.value
        
    
    @_check_connection
    def set_target_value(self, value):
        self.dilsc.setParameter(f'mf{self.direction}', 'target', value)
            
    @_check_connection
    def is_moving(self):
        response = self.dilsc.getParameter(f'mf{self.direction}','status', trycache=False)
        return response[0][0] > StatusType.PREPARED

    