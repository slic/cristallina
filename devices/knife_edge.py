from slic.core.device.simpledevice import SimpleDevice
from slic.devices.general.motor import Motor

mot_x = Motor("SAR-EXPMX:MOT_FX")
mot_y = Motor("SAR-EXPMX:MOT_FY")

KnifeEdge = SimpleDevice("2D Knife edge stage", x=mot_x, y=mot_y)
