from slic.core.adjustable import PVAdjustable
from slic.utils import typename


class AlignmentLaser:
    """Class for the alignment laser"""

    def __init__(self, ID, name="Cristallina alignment laser"):
        self.ID = ID
        self.name = name or ID
        pvname_setvalue = ID
        pvname_readback = ID
        self._adj = PVAdjustable(pvname_setvalue, pvname_readback, accuracy=0, internal=True)

    def set_in(self):
        self._adj.set_target_value(-19).wait()

    def set_out(self):
        self._adj.set_target_value(-1).wait()

    @property
    def status(self):
        state = self._adj.get_current_value()
        if state is None:
            return "not connected"
        elif state < -18:
            return "In"
        elif state > -6:
            return "Out"
        else:
            return "in an undefined position"

    # TODO: Save the status downstream of the beamline when laser is out before bringing it back in
    def get_downstream_status(self):
        return

    def __repr__(self):
        tn = typename(self)
        return f'{tn} "{self.name}" is {self.status}'
