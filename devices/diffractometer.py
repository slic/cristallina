""" Diffractometer

    motorized axis: 

    SARES30-CPCL-ECMC02:ROT2THETA
    SARES30-CPCL-ECMC02:ROTTHETA
    SARES30-CPCL-ECMC02:TRXBASE
    SARES30-CPCL-ECMC02:TRYBASE

"""

from slic.core.adjustable import Adjustable, Linked

from slic.core.device import Device, SimpleDevice
from slic.devices.general.motor import Motor


class Diffractometer(Device):
    def __init__(self, ID, motor_naming="MOTOR", **kwargs):
        super().__init__(ID, **kwargs)
        
        self.twotheta = Motor("SARES30-CPCL-ECMC02:ROT2THETA")  # , ID=None, name=None, units=None, internal=False):
        self.theta = Motor("SARES30-CPCL-ECMC02:ROTTHETA")      # , ID=None, name=None, units=None, internal=False):
        
        self.trx_base = Motor("SARES30-CPCL-ECMC02:TRXBASE")     # , ID=None, name=None, units=None, internal=False):
        self.try_base = Motor("SARES30-CPCL-ECMC02:TRYBASE")     # , ID=None, name=None, units=None, internal=False):

        self.tr_x = Motor("SARES30-CPCL-ECMC02:TRX")
        self.tr_y = Motor("SARES30-CPCL-ECMC02:TRY")
        self.tr_z = Motor("SARES30-CPCL-ECMC02:TRZ")

        self.td = Motor("SARES30-CPCL-ECMC02:TD")
        

# Set speed: 
# diffractometer.theta._motor.VELO = 0.25

class ThetasCombined(Linked):
    def __init__(self, *args, **kwargs):
        super().__init__(self, *args, **kwargs)

    def connect_axis(self):
        """
         calculate offset to match scale factor
        """

        offset = self.secondary.get_current_value() - self.primary.get_current_value() * self.scale_factor
        self.offset = offset


