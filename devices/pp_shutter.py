from slic.core.adjustable import PVAdjustable
from slic.utils import typename


class PP_Shutter:
    """Class for when pulse picker is used as a shutter. Opening and closing is done with turning on and off the event reciever universal output ON and OFF."""

    def __init__(self, ID, name="X-ray Pulse Picker Shutter"):
        self.ID = ID
        self.name = name or ID
        pvname_setvalue = ID
        pvname_readback = ID
        self._adj = PVAdjustable(pvname_setvalue, pvname_readback, accuracy=0, internal=True)

    def close(self):
        self._adj.set_target_value(0).wait()

    def open(self):
        self._adj.set_target_value(1).wait()

    @property
    def status(self):
        state = self._adj.get_current_value()
        if state is None:
            return "not connected"
        return "open" if state else "closed"

    def __repr__(self):
        tn = typename(self)
        return f'{tn} "{self.name}" is {self.status}'
