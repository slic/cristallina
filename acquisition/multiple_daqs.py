""" Multiple DAQs with different rates"""

from slic.core.acquisition import SFAcquisition

rates = {'100Hz': 1, '50Hz': 2, '25Hz': 4, '10Hz': 10, '5Hz': 20, '1Hz': 100}

def generate_DAQS(instrument, pgroup,bs_channels, pvs, detectors):
    DAQS = {name:SFAcquisition(instrument, pgroup, default_channels=bs_channels, default_pvs=pvs, default_detectors=detectors, rate_multiplicator=r) for (name, r) in rates.items()}
    return DAQS

