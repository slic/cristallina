from slic.utils.hastyepics import get_pv as PV
from slic.core.device.basedevice import BaseDevice
from slic.core.adjustable import PVAdjustable
from slic.utils import typename

CTA_sequence_start_PID = PV("SAR-CCTA-ESC:seq0Ctrl-StartedAt-O")


class PP_Shutter:
    """Pulsed magnet instrument classs"""

    def __init__(self, ID, name="Pulsed Magnet Instrument"):
        self.ID = ID
        self.name = name or ID
        pvname_setvalue = ID
        pvname_readback = ID
        self._adj = PVAdjustable(pvname_setvalue, pvname_readback, accuracy=0, internal=True)

    def close(self):
        self._adj.set_target_value(0).wait()

    def open(self):
        self._adj.set_target_value(1).wait()

    @property
    def status(self):
        state = self._adj.get_current_value()
        if state is None:
            return "not connected"
        return "open" if state else "closed"

    def __repr__(self):
        tn = typename(self)
        return f'{tn} "{self.name}" is {self.status}'
