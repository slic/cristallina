# Channels to save at Cristallina endstation, Bill's temporary implementations

##########################################################################################################
##########################################################################################################
##########################################################################################################
# JF configurations

from slic.core.acquisition.detcfg import DetectorConfig

detectors = [
    "JF16T03V01",
]

detectors_with_config = DetectorConfig(detectors)
#detectors_with_config["JF16T03V01"]['adc_to_energy'] = True
#detectors_with_config["JF16T03V01"]['factor'] = 0.25
#detectors_with_config["JF16T03V01"]['save_dap_results'] = False
#detectors_with_config["JF16T03V01"]['disabled_modules'] = [0, 1] # bottom module:0, middle module:1, top module:2
ROIs = {'direct_beam': [200,400,500,800]}
detectors_with_config["JF16T03V01"]['roi'] = ROIs
#detectors_with_config["JF16T03V01"]['remove_raw_files'] = True

##########################################################################################################
##########################################################################################################
##########################################################################################################
# BS channels, updated 05.11.2023 from official channel list and adapted

camera_channels = [
    # "SARES30-CAMS156-PCO1:FPICTURE",       # PCO edge camera for the wavefront analysis (from Alvra)
    # "SARES30-CAMS156-SMX-OAV:FPICTURE",    # SwissMX OAV camera picture
    # "SARES30-CAMS156-XE:FPICTURE",         # X-ray eye
]

####################
# Machine gas intensity monitor
channels_gas_monitor = [
    "SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-AVG",
    # "SARFE10-PBPG050:SLOW-X",
    # "SARFE10-PBPG050:SLOW-Y",
    "SARFE10-PBIG050-EVR0:CALCI",  # good for correlations with total beam intensity
    "SARFE10-PBPG050:HAMP-INTENSITY-CAL",
]

# RF phases and amplitudes
channels_RF = [
    "SINSB01-RLLE-DSP:PHASE-VS",
    "SINSB02-RLLE-DSP:PHASE-VS",
    "SINSB03-RLLE-DSP:PHASE-VS",
    "SINSB04-RLLE-DSP:PHASE-VS",
    "SINXB01-RLLE-DSP:PHASE-VS",
    "SINDI01-RLLE-DSP:PHASE-VS",
    "S10CB01-RLLE-DSP:PHASE-VS",
    "S10CB02-RLLE-DSP:PHASE-VS",
    "S10CB03-RLLE-DSP:PHASE-VS",
    "S10CB04-RLLE-DSP:PHASE-VS",
    "S10CB05-RLLE-DSP:PHASE-VS",
    "S10CB06-RLLE-DSP:PHASE-VS",
    "S10CB07-RLLE-DSP:PHASE-VS",
    "S10CB08-RLLE-DSP:PHASE-VS",
    "S10CB09-RLLE-DSP:PHASE-VS",
    "S20CB01-RLLE-DSP:PHASE-VS",
    "S20CB02-RLLE-DSP:PHASE-VS",
    "S20CB03-RLLE-DSP:PHASE-VS",
    "S20CB04-RLLE-DSP:PHASE-VS",
    "S30CB01-RLLE-DSP:PHASE-VS",
    "S30CB02-RLLE-DSP:PHASE-VS",
    "S30CB03-RLLE-DSP:PHASE-VS",
    "S30CB04-RLLE-DSP:PHASE-VS",
    "S30CB05-RLLE-DSP:PHASE-VS",
    "S30CB06-RLLE-DSP:PHASE-VS",
    "S30CB07-RLLE-DSP:PHASE-VS",
    "S30CB08-RLLE-DSP:PHASE-VS",
    "S30CB09-RLLE-DSP:PHASE-VS",
    "S30CB10-RLLE-DSP:PHASE-VS",
    "S30CB11-RLLE-DSP:PHASE-VS",
    "S30CB12-RLLE-DSP:PHASE-VS",
    "S30CB13-RLLE-DSP:PHASE-VS",
    "S30CB14-RLLE-DSP:PHASE-VS",
    "SINEG01-RLLE-DSP:AMPLT-VS",
    "SINSB01-RLLE-DSP:AMPLT-VS",
    "SINSB02-RLLE-DSP:AMPLT-VS",
    "SINSB03-RLLE-DSP:AMPLT-VS",
    "SINSB04-RLLE-DSP:AMPLT-VS",
    "SINXB01-RLLE-DSP:AMPLT-VS",
    "SINDI01-RLLE-DSP:AMPLT-VS",
    "S10CB01-RLLE-DSP:AMPLT-VS",
    "S10CB02-RLLE-DSP:AMPLT-VS",
    "S10CB03-RLLE-DSP:AMPLT-VS",
    "S10CB04-RLLE-DSP:AMPLT-VS",
    "S10CB05-RLLE-DSP:AMPLT-VS",
    "S10CB06-RLLE-DSP:AMPLT-VS",
    "S10CB07-RLLE-DSP:AMPLT-VS",
    "S10CB08-RLLE-DSP:AMPLT-VS",
    "S10CB09-RLLE-DSP:AMPLT-VS",
    "S20CB01-RLLE-DSP:AMPLT-VS",
    "S20CB02-RLLE-DSP:AMPLT-VS",
    "S20CB03-RLLE-DSP:AMPLT-VS",
    "S20CB04-RLLE-DSP:AMPLT-VS",
    "S30CB01-RLLE-DSP:AMPLT-VS",
    "S30CB02-RLLE-DSP:AMPLT-VS",
    "S30CB03-RLLE-DSP:AMPLT-VS",
    "S30CB04-RLLE-DSP:AMPLT-VS",
    "S30CB05-RLLE-DSP:AMPLT-VS",
    "S30CB06-RLLE-DSP:AMPLT-VS",
    "S30CB07-RLLE-DSP:AMPLT-VS",
    "S30CB08-RLLE-DSP:AMPLT-VS",
    "S30CB09-RLLE-DSP:AMPLT-VS",
    "S30CB10-RLLE-DSP:AMPLT-VS",
    "S30CB11-RLLE-DSP:AMPLT-VS",
    "S30CB12-RLLE-DSP:AMPLT-VS",
    "S30CB13-RLLE-DSP:AMPLT-VS",
    "S30CB14-RLLE-DSP:AMPLT-VS",
]

channels_Xeye = [
    "SARES30-CAMS156-XE:intensity",
#    "SARES30-CAMS156-XE:x_center_of_mass",
    "SARES30-CAMS156-XE:x_fit_amplitude",
    "SARES30-CAMS156-XE:x_fit_mean",
    "SARES30-CAMS156-XE:x_fit_offset",
    "SARES30-CAMS156-XE:x_fit_standard_deviation",
#    "SARES30-CAMS156-XE:x_fwhm",
    "SARES30-CAMS156-XE:x_profile",
#    "SARES30-CAMS156-XE:x_rms",
#    "SARES30-CAMS156-XE:y_center_of_mass",
    "SARES30-CAMS156-XE:y_fit_amplitude",
    "SARES30-CAMS156-XE:y_fit_mean",
    "SARES30-CAMS156-XE:y_fit_offset",
    "SARES30-CAMS156-XE:y_fit_standard_deviation",
#    "SARES30-CAMS156-XE:y_fwhm",
    "SARES30-CAMS156-XE:y_profile",
#    "SARES30-CAMS156-XE:y_rms",
    "SARES30-CAMS156-XE:FPICTURE",    
]

######################
# PBPS053
channels_PBPS053 = [
    "SARFE10-PBPS053:INTENSITY",
    "SARFE10-PBPS053:XPOS",
    "SARFE10-PBPS053:YPOS",
]

####################
# PSSS059
channels_PSSS059 = [
    "SARFE10-PSSS059:FIT-BRT",
    "SARFE10-PSSS059:FIT-COM",
    "SARFE10-PSSS059:FIT-FWHM",
    "SARFE10-PSSS059:FIT-RES",
    "SARFE10-PSSS059:FIT-RMS",
    "SARFE10-PSSS059:SPECT-COM",
    "SARFE10-PSSS059:SPECT-RES",
    "SARFE10-PSSS059:SPECT-RMS",
    "SARFE10-PSSS059:SPECTRUM_Y_SUM",
    "SARFE10-PSSS059:SPECTRUM_X",
    "SARFE10-PSSS059:SPECTRUM_Y",
    "SARFE10-PSSS059:processing_parameters",
]

channels_PSSS059_camera = [
    "SARFE10-PSSS059:FPICTURE",
]

####################
# PSSS059
channels_PSSS059_LB = [
    "SARFE10-PSSS059-LB:FIT-BRT",
    "SARFE10-PSSS059-LB:FIT-COM",
    "SARFE10-PSSS059-LB:FIT-FWHM",
    "SARFE10-PSSS059-LB:FIT-RES",
    "SARFE10-PSSS059-LB:FIT-RMS",
    "SARFE10-PSSS059-LB:SPECT-COM",
    "SARFE10-PSSS059-LB:SPECT-RES",
    "SARFE10-PSSS059-LB:SPECT-RMS",
    "SARFE10-PSSS059-LB:SPECTRUM_X",
    "SARFE10-PSSS059-LB:SPECTRUM_Y",
    "SARFE10-PSSS059-LB:SPECTRUM_Y_SUM",
    "SARFE10-PSSS059-LB:processing_parameters",
]

channels_PSSS059_LB_camera = [
    "SARFE10-PSSS059-LB:FPICTURE",
]


###################################
## Bernina channels
# Beam position monitor PBPS113
channels_Bernina = [
    "SAROP21-PBPS103:INTENSITY",
    "SAROP21-PBPS103:XPOS",
    "SAROP21-PBPS103:YPOS",
    "SAROP21-PPRM113:FPICTURE",
    "SAROP21-PPRM113:intensity",
    "SAROP21-PPRM113:x_fit_mean",
    "SAROP21-PPRM113:y_fit_mean",
]

###################################
# Beam position monitor PBPS113
channels_PBPS113 = [
    "SAROP31-PBPS113:INTENSITY",
    "SAROP31-PBPS113:INTENSITY_UJ",
    "SAROP31-PBPS113:Lnk9Ch0-PP_VAL_PD0",
    "SAROP31-PBPS113:Lnk9Ch0-PP_VAL_PD1",
    "SAROP31-PBPS113:Lnk9Ch0-PP_VAL_PD2",
    "SAROP31-PBPS113:Lnk9Ch0-PP_VAL_PD3",
    "SAROP31-PBPS113:XPOS",
    "SAROP31-PBPS113:YPOS",
]
channels_PBPS113_waveforms = [
    "SAROP31-PBPS113:Lnk9Ch0-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch1-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch2-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch3-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch4-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch5-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch6-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch7-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch8-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch9-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch10-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch11-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch12-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch13-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch14-WF-DATA",
    "SAROP31-PBPS113:Lnk9Ch15-WF-DATA",
]

###################################
# Diode PDIM 113
channels_PDIM113 = [
    "SAROP31-PBPS113:Lnk9Ch0-PP_VAL_PD4",
]


####################
# Profile monitor PPRM113 (from _proc process)
channels_PPRM113 = [
    "SAROP31-PPRM113:intensity",
    "SAROP31-PPRM113:x_center_of_mass",
    "SAROP31-PPRM113:x_fit_amplitude",
    "SAROP31-PPRM113:x_fit_mean",
    "SAROP31-PPRM113:x_fit_offset",
    "SAROP31-PPRM113:x_fit_standard_deviation",
    "SAROP31-PPRM113:x_fwhm",
    "SAROP31-PPRM113:x_profile",
    "SAROP31-PPRM113:x_rms",
    "SAROP31-PPRM113:y_center_of_mass",
    "SAROP31-PPRM113:y_fit_amplitude",
    "SAROP31-PPRM113:y_fit_mean",
    "SAROP31-PPRM113:y_fit_offset",
    "SAROP31-PPRM113:y_fit_standard_deviation",
    "SAROP31-PPRM113:y_fwhm",
    "SAROP31-PPRM113:y_profile",
    "SAROP31-PPRM113:y_rms",
    # "SAROP31-PPRM113:FPICTURE",                         # full pictures for debugging purposes at the moment, from _ib process
]

###########################
# Beam position monitor PBPS149
# "SARES30-CAMS156-PCO1:FPICTURE",       # PCO edge camera for the wavefront analysis (from Alvra)
# "SARES30-CAMS156-SMX-OAV:FPIC

###########################
# Beam position monitor
channels_PBPS149 = [
    "SAROP31-PBPS149:INTENSITY",
    "SAROP31-PBPS149:INTENSITY_UJ",
    "SAROP31-PBPS149:Lnk9Ch0-PP_VAL_PD0",
    "SAROP31-PBPS149:Lnk9Ch0-PP_VAL_PD1",
    "SAROP31-PBPS149:Lnk9Ch0-PP_VAL_PD2",
    "SAROP31-PBPS149:Lnk9Ch0-PP_VAL_PD3",
    "SAROP31-PBPS149:XPOS",
    "SAROP31-PBPS149:YPOS",
]

#######################
# Profile monitor PPRM150 (from _proc process)
channels_PPRM150 = [
    "SAROP31-PPRM150:intensity",
    "SAROP31-PPRM150:x_center_of_mass",
    "SAROP31-PPRM150:x_fit_amplitude",
    "SAROP31-PPRM150:x_fit_mean",
    "SAROP31-PPRM150:x_fit_offset",
    "SAROP31-PPRM150:x_fit_standard_deviation",
    "SAROP31-PPRM150:x_fwhm",
    "SAROP31-PPRM150:x_profile",
    "SAROP31-PPRM150:x_rms",
    "SAROP31-PPRM150:y_center_of_mass",
    "SAROP31-PPRM150:y_fit_amplitude",
    "SAROP31-PPRM150:y_fit_mean",
    "SAROP31-PPRM150:y_fit_offset",
    "SAROP31-PPRM150:y_fit_standard_deviation",
    "SAROP31-PPRM150:y_fwhm",
    "SAROP31-PPRM150:y_profile",
    "SAROP31-PPRM150:y_rms",
    # "SAROP31-PPRM150:FPICTURE",                         # full pictures for debugging purposes at the moment, from _ib process
]


###################################
# Diode of PSCD153
channels_PSCD153_diode = [
    "SAROP31-PBPS149:Lnk9Ch0-PP_VAL_PD4",
]

#######################
# Cristallina event reciever
channels_EVR = [
    "SAR-CVME-TIFALL6:EvtSet",
]

channels_Xeye = [
    "SARES30-CAMS156-XE:intensity",
#    "SARES30-CAMS156-XE:x_center_of_mass",
    "SARES30-CAMS156-XE:x_fit_amplitude",
    "SARES30-CAMS156-XE:x_fit_mean",
    "SARES30-CAMS156-XE:x_fit_offset",
    "SARES30-CAMS156-XE:x_fit_standard_deviation",
#    "SARES30-CAMS156-XE:x_fwhm",
    "SARES30-CAMS156-XE:x_profile",
#    "SARES30-CAMS156-XE:x_rms",
#    "SARES30-CAMS156-XE:y_center_of_mass",
    "SARES30-CAMS156-XE:y_fit_amplitude",
    "SARES30-CAMS156-XE:y_fit_mean",
    "SARES30-CAMS156-XE:y_fit_offset",
    "SARES30-CAMS156-XE:y_fit_standard_deviation",
#    "SARES30-CAMS156-XE:y_fwhm",
    "SARES30-CAMS156-XE:y_profile",
#    "SARES30-CAMS156-XE:y_rms",
    "SARES30-CAMS156-XE:FPICTURE",    
]

#######################
# Digitizer
channels_digitizer = [
    "SARES30-LTIM01-EVR0:DUMMY_PV1_NBS",
    "SARES30-LTIM01-EVR0:DUMMY_PV2_NBS",
    "SARES30-LTIM01-EVR0:DUMMY_PV3_NBS",
    "SARES30-LTIM01-EVR0:DUMMY_PV4_NBS",
    "SARES30-LSCP1-FNS:CH0:VAL_GET",  # Signal-Background
    "SARES30-LSCP1-CRISTA1:CH0:1",  # Waveform signal
    "SARES30-LSCP1-CRISTA1:CH2:1",  # Waveform trigger
    "SARES30-LTIM01-EVR0:CALCI",  # Calculated intensity
]


##########################################################################################################
# Bernina channels

#######################
# Profile monitor PPRM113 (from _proc process)
channels_PPRM113_Bernina = [
    "SAROP21-PPRM113:intensity",
#    "SAROP21-PPRM113:x_center_of_mass",
#    "SAROP21-PPRM113:x_fit_amplitude",
    "SAROP21-PPRM113:x_fit_mean",
#    "SAROP21-PPRM113:x_fit_offset",
#    "SAROP21-PPRM113:x_fit_standard_deviation",
    "SAROP21-PPRM113:x_fwhm",
#    "SAROP21-PPRM113:x_profile",
#    "SAROP21-PPRM113:x_rms",
#    "SAROP21-PPRM113:y_center_of_mass",
#    "SAROP21-PPRM113:y_fit_amplitude",
    "SAROP21-PPRM113:y_fit_mean",
#    "SAROP21-PPRM113:y_fit_offset",
#    "SAROP21-PPRM113:y_fit_standard_deviation",
    "SAROP21-PPRM113:y_fwhm",
#    "SAROP21-PPRM113:y_profile",
#    "SAROP21-PPRM113:y_rms",
    # "SAROP21-PPRM113:FPICTURE",                         # full pictures for debugging purposes at the moment, from _ib process
]

###########################
# Beam position monitor Bernina
channels_PBPS103_Bernina = [
    "SAROP21-PBPS103:INTENSITY",
    "SAROP21-PBPS103:XPOS",
    "SAROP21-PBPS103:YPOS",
]

##########################################################################################################
# special bs channel configurations

####################

bs_channels_OAPU107_scan = (
    channels_gas_monitor
    + channels_PBPS053
    + channels_PSSS059
    + channels_PBPS113
    + channels_PBPS149
    + channels_PPRM150
)

bs_channels_OAPU149_scan = (
    channels_gas_monitor
    + channels_PBPS053
    + channels_PSSS059
    + channels_PBPS113
    + channels_PBPS149
    + channels_PPRM150
)

bs_channels_pbps_snapshot = (
    channels_gas_monitor
    + channels_PBPS053
    + channels_PSSS059
    + channels_PBPS113
    + channels_PBPS149
)

bs_channels_PBPS_tests = (
    channels_gas_monitor
    + channels_PBPS053
    + channels_PSSS059
    + channels_PBPS113
    + channels_PBPS149
    + channels_Xeye
)

bs_channels_OATA_tests = (
    channels_gas_monitor
    + channels_PBPS053
#    + channels_PSSS059
#    + channels_PBPS113
#    + channels_PBPS149
    + channels_PSCD153_diode
)

bs_channels_OATA_tests_JF = (
    channels_gas_monitor
    + channels_PBPS053
    + channels_PSSS059
    + channels_PBPS113
    + channels_PBPS149
)

bs_channels_PPRM150 = (
    channels_gas_monitor
    + channels_PBPS053
    + channels_PSSS059
    + channels_PPRM150
)

bs_channels_jf_direct_beam = (
    channels_gas_monitor
    + channels_PBPS053
    + channels_PSSS059
    + channels_PBPS113
    + channels_PBPS149
    + channels_EVR
)

bs_channels_DCM_Bernina = (
    channels_gas_monitor
    + channels_PBPS053
    + channels_PSSS059
    + channels_PPRM113_Bernina
    + channels_PBPS103_Bernina
)

bs_channels_PSSS = (
    channels_gas_monitor
    + channels_PBPS053
    + channels_PSSS059
    + channels_PSSS059_camera
    + channels_PBPS113
    + channels_PBPS149
)

bs_channels_PSSS_LB = (
    channels_gas_monitor
    + channels_PBPS053
    + channels_PSSS059_LB
    + channels_PSSS059_LB_camera
    + channels_PBPS113
    + channels_PBPS149
)


##########################################################################################################
##########################################################################################################
##########################################################################################################
# Epics PVS

# Compression, charge settings
#######################
# Machine
pvs_machine = [
    "SARCL02-MBND100:P-READ",  # Predicted bunch energy
    "SARUN:FELPHOTENE.VAL",  # Predicted photon energy from machine settings
    "SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-AVG.VAL",  # Average pulse energy from the gas detector
]
pvs_RF = [
    "SINSB01-RSYS:GET-VSUM-PHASE-OFFSET",
    "SINSB02-RSYS:GET-VSUM-PHASE-OFFSET",
    "SINSB03-RSYS:GET-VSUM-PHASE-OFFSET",
    "SINSB04-RSYS:GET-VSUM-PHASE-OFFSET",
    "SINXB01-RSYS:GET-VSUM-PHASE-OFFSET",
    "SINDI01-RSYS:GET-VSUM-PHASE-OFFSET",
    "S10CB01-RSYS:GET-VSUM-PHASE-OFFSET",
    "S10CB02-RSYS:GET-VSUM-PHASE-OFFSET",
    "S10CB03-RSYS:GET-VSUM-PHASE-OFFSET",
    "S10CB04-RSYS:GET-VSUM-PHASE-OFFSET",
    "S10CB06-RSYS:GET-VSUM-PHASE-OFFSET",
    "S10CB05-RSYS:GET-VSUM-PHASE-OFFSET",
    "S10CB07-RSYS:GET-VSUM-PHASE-OFFSET",
    "S10CB08-RSYS:GET-VSUM-PHASE-OFFSET",
    "S10CB09-RSYS:GET-VSUM-PHASE-OFFSET",
    "S20CB01-RSYS:GET-VSUM-PHASE-OFFSET",
    "S20CB02-RSYS:GET-VSUM-PHASE-OFFSET",
    "S20CB03-RSYS:GET-VSUM-PHASE-OFFSET",
    "S20CB04-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB01-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB02-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB03-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB04-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB05-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB06-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB07-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB08-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB09-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB10-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB11-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB12-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB13-RSYS:GET-VSUM-PHASE-OFFSET",
    "S30CB14-RSYS:GET-VSUM-PHASE-OFFSET",
    "SINEG01-RSYS:GET-VSUM-AMPLT-SCALE",
    "SINSB01-RSYS:GET-VSUM-AMPLT-SCALE",
    "SINSB02-RSYS:GET-VSUM-AMPLT-SCALE",
    "SINSB03-RSYS:GET-VSUM-AMPLT-SCALE",
    "SINSB04-RSYS:GET-VSUM-AMPLT-SCALE",
    "SINXB01-RSYS:GET-VSUM-AMPLT-SCALE",
    "SINDI01-RSYS:GET-VSUM-AMPLT-SCALE",
    "S10CB01-RSYS:GET-VSUM-AMPLT-SCALE",
    "S10CB02-RSYS:GET-VSUM-AMPLT-SCALE",
    "S10CB03-RSYS:GET-VSUM-AMPLT-SCALE",
    "S10CB04-RSYS:GET-VSUM-AMPLT-SCALE",
    "S10CB05-RSYS:GET-VSUM-AMPLT-SCALE",
    "S10CB06-RSYS:GET-VSUM-AMPLT-SCALE",
    "S10CB07-RSYS:GET-VSUM-AMPLT-SCALE",
    "S10CB08-RSYS:GET-VSUM-AMPLT-SCALE",
    "S10CB09-RSYS:GET-VSUM-AMPLT-SCALE",
    "S20CB01-RSYS:GET-VSUM-AMPLT-SCALE",
    "S20CB02-RSYS:GET-VSUM-AMPLT-SCALE",
    "S20CB03-RSYS:GET-VSUM-AMPLT-SCALE",
    "S20CB04-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB01-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB02-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB03-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB04-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB05-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB06-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB07-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB08-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB09-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB10-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB11-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB12-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB13-RSYS:GET-VSUM-AMPLT-SCALE",
    "S30CB14-RSYS:GET-VSUM-AMPLT-SCALE",
]

#######################
# Undulator gap
pvs_undulator = [
    "SARUN03-UIND030:K_SET.VAL",
    "SARUN04-UIND030:K_SET.VAL",
    "SARUN05-UIND030:K_SET.VAL",
    "SARUN06-UIND030:K_SET.VAL",
    "SARUN07-UIND030:K_SET.VAL",
    "SARUN08-UIND030:K_SET.VAL",
    "SARUN09-UIND030:K_SET.VAL",
    "SARUN10-UIND030:K_SET.VAL",
    "SARUN11-UIND030:K_SET.VAL",
    "SARUN12-UIND030:K_SET.VAL",
    "SARUN13-UIND030:K_SET.VAL",
    "SARUN14-UIND030:K_SET.VAL",
    "SARUN15-UIND030:K_SET.VAL",
]

####################
# Machine gas intensity monitor
pvs_gas_monitor = [
    "SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-US",
    "SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-DS",
]

#####################
# Slits OAPU044
pvs_OAPU044 = [
    "SARFE10-OAPU044:MOTOR_X",
    "SARFE10-OAPU044:MOTOR_Y",
    "SARFE10-OAPU044:MOTOR_W",
    "SARFE10-OAPU044:MOTOR_H",
]

###################
# Beam position monitor PBPS053
pvs_PBPS053 = [
    "SARFE10-PBPS053:MOTOR_X1",
    "SARFE10-PBPS053:MOTOR_X2",
    "SARFE10-PBPS053:MOTOR_PROBE",
]

###################
# Spectrometer PSSS059
pvs_PSSS059 = [
    "SARFE10-PSSS055:MOTOR_X1.RBV",
    "SARFE10-PSSS055:MOTOR_Y1.RBV",
    "SARFE10-PSSS055:MOTOR_ROT_X1.RBV",
    "SARFE10-PSSS055:MOTOR_PROBE.RBV",
    "SARFE10-PSSS059:MOTOR_X3.RBV",
    "SARFE10-PSSS059:MOTOR_Y3.RBV",
    "SARFE10-PSSS059:MOTOR_ROT_X3.RBV",
    "SARFE10-PSSS059:MOTOR_Y4.RBV",
    "SARFE10-PSSS059:MOTOR_ROT_X4.RBV",
    "SARFE10-PSSS059:MOTOR_X5.RBV",
    "SARFE10-PSSS059:MOTOR_Y5.RBV",
    "SARFE10-PSSS059:MOTOR_Z5.RBV",
    "SARFE10-PSSS055:GRATING_SP",
    "SARFE10-PSSS059:CRYSTAL_SP",
    "SARFE10-PSSS059:SPC_ROI_YMIN",
    "SARFE10-PSSS059:SPC_ROI_YMAX",
]

####################
# Upstream attenuator OATT053
pvs_OATT053 = [
    "SARFE10-OATT053:MOTOR_1",
    "SARFE10-OATT053:MOTOR_1.RBV",
    "SARFE10-OATT053:MOTOR_2",
    "SARFE10-OATT053:MOTOR_2.RBV",
    "SARFE10-OATT053:MOTOR_3",
    "SARFE10-OATT053:MOTOR_3.RBV",
    "SARFE10-OATT053:MOTOR_4",
    "SARFE10-OATT053:MOTOR_4.RBV",
    "SARFE10-OATT053:MOTOR_5",
    "SARFE10-OATT053:MOTOR_5.RBV",
    "SARFE10-OATT053:MOTOR_6",
    "SARFE10-OATT053:MOTOR_6.RBV",
    "SARFE10-OATT053:ENERGY",
    "SARFE10-OATT053:TRANS_SP",
    "SARFE10-OATT053:TRANS_RB",
]

###################
# Beam profile monitor PPRM053
pvs_PPRM053 = [
    "SARFE10-PPRM053:MOTOR_PROBE.RBV",
]

####################
# First Cristallina horizontal offset mirror OOMH067
pvs_OOMH067 = [
    "SAROP31-OOMH067:W_X.RBV",
    "SAROP31-OOMH067:W_Y.RBV",
    "SAROP31-OOMH067:W_RX.RBV",
    "SAROP31-OOMH067:W_RY.RBV",
    "SAROP31-OOMH067:W_RZ.RBV",
    "SAROP31-OOMH067:BU.RBV",
    "SAROP31-OOMH067:BD.RBV",
    "SAROP31-OOMH067:VS1.RBV",
    "SAROP31-OOMH067:VS2.RBV",
    "SAROP31-OOMH067:VS3.RBV",
    "SAROP31-OOMH067:TX.RBV",
    "SAROP31-OOMH067:RY.RBV",
]

####################
# Beam screen between the first two horizontal mirrors PSCR068
pvs_PSCR068 = [
    "SAROP31-PSCR068:MOTOR_PROBE.RBV",
]

####################
# Second Cristallina horizontal offset mirror OOMH084
pvs_OOMH084 = [
    "SAROP31-OOMH084:W_X.RBV",
    "SAROP31-OOMH084:W_Y.RBV",
    "SAROP31-OOMH084:W_RX.RBV",
    "SAROP31-OOMH084:W_RY.RBV",
    "SAROP31-OOMH084:W_RZ.RBV",
    "SAROP31-OOMH084:BU.RBV",
    "SAROP31-OOMH084:BD.RBV",
    "SAROP31-OOMH084:VS1.RBV",
    "SAROP31-OOMH084:VS2.RBV",
    "SAROP31-OOMH084:VS3.RBV",
    "SAROP31-OOMH084:TX.RBV",
    "SAROP31-OOMH084:RY.RBV",
]

###################
# Beam profile monitor PPRM085
pvs_PPRM085 = [
    "SAROP31-PPRM085:MOTOR_PROBE.RBV",
]

###################
# Slits OAPU107
pvs_OAPU107 = [
    "SAROP31-OAPU107:MOTOR_X.VAL",
    "SAROP31-OAPU107:MOTOR_X.RBV",
    "SAROP31-OAPU107:MOTOR_Y.VAL",
    "SAROP31-OAPU107:MOTOR_Y.RBV",
]

###################
## Beam position and intensity monitor PBPS113
pvs_PBPS113 = [
    "SAROP31-PBPS113:MOTOR_X1.DRBV",
    "SAROP31-PBPS113:MOTOR_Y1.DRBV",
    "SAROP31-PBPS113:MOTOR_X1.RBV",
    "SAROP31-PBPS113:MOTOR_Y1.RBV",
    "SAROP31-PBPS113:MOTOR_PROBE.RBV",
]

####################
# Diode PDIM113
pvs_PDIM113 = [
    "SAROP31-PDIM113:MOTOR_PROBE.RBV"
]

###################
# Beam profile monitor PPRM113
pvs_PPRM113 = [
    "SAROP31-PPRM113:MOTOR_PROBE.RBV",
]

####################
# Alignment laser mirror OLAS147
pvs_OLAS147 = [
    "SAROP31-OLAS147:MOTOR_1.RBV",
]

###################
# Slits OAPU149
pvs_OAPU149 = [
    "SAROP31-OAPU149:MOTOR_X.RBV",
    "SAROP31-OAPU149:MOTOR_Y.RBV",
    "SAROP31-OAPU149:MOTOR_W.RBV",
    "SAROP31-OAPU149:MOTOR_H.RBV",
]

###################
# Beam position and intensity monitor PBPS149
pvs_PBPS149 = [
    "SAROP31-PBPS149:MOTOR_X1.DRBV",
    "SAROP31-PBPS149:MOTOR_Y1.DRBV",
    "SAROP31-PBPS149:MOTOR_X1.RBV",
    "SAROP31-PBPS149:MOTOR_Y1.RBV",
    "SAROP31-PBPS149:MOTOR_PROBE.RBV",
]

###################
# Beam profile monitor PPRM150
pvs_PPRM150 = [
    "SAROP31-PPRM150:MOTOR_PROBE.RBV",
]

####################
# Attenuators OATA150
pvs_OATA150 = [
    "SAROP31-OATA150:MOTOR_1.RBV",
    "SAROP31-OATA150:MOTOR_2.RBV",
    "SAROP31-OATA150:MOTOR_3.RBV",
    "SAROP31-OATA150:MOTOR_4.RBV",
    "SAROP31-OATA150:MOTOR_5.RBV",
    "SAROP31-OATA150:MOTOR_6.RBV",
    "SAROP31-OATA150:ENERGY",
    "SAROP31-OATA150:TRANS_SP",
    "SAROP31-OATA150:TRANS_RB",
    "SAROP31-OATA150:TRANS3EDHARM_RB",
    "SAROP31-OATA150:MOT2TRANS.VALD"
]

####################
# Pulse picker OPPI151
pvs_OPPI151 = [
    "SAROP31-OPPI151:MOTOR_X.RBV",
    "SAROP31-OPPI151:MOTOR_Y.RBV",
]


####################
## Horizontal offset mirror ODMV152
pvs_ODMV152 = [
    "SAROP31-ODMV152:W_X.RBV",
    "SAROP31-ODMV152:W_Y.RBV",
    "SAROP31-ODMV152:W_RX.RBV",
    "SAROP31-ODMV152:W_RZ.RBV",
    "SAROP31-ODMV152:BU.RBV",
    "SAROP31-ODMV152:BD.RBV",
    "SAROP31-ODMV152:VS1.RBV",
    "SAROP31-ODMV152:VS2.RBV",
    "SAROP31-ODMV152:VS3.RBV",
    "SAROP31-ODMV152:TX.RBV",
]

###########################
# Vertical KB mirror OKBV153
pvs_OKBV153 = [
    "SAROP31-OKBV153:W_X.RBV",
    "SAROP31-OKBV153:W_Y.RBV",
    "SAROP31-OKBV153:W_RX.RBV",
    "SAROP31-OKBV153:W_RY.RBV",
    "SAROP31-OKBV153:W_RZ.RBV",
    "SAROP31-OKBV153:BU.RBV",
    "SAROP31-OKBV153:BD.RBV",
    "SAROP31-OKBV153:TY1.RBV",
    "SAROP31-OKBV153:TY2.RBV",
    "SAROP31-OKBV153:TY3.RBV",
    "SAROP31-OKBV153:TX1.RBV",
    "SAROP31-OKBV153:TX2.RBV",
]

####################
# Screen between the KB's PSCD153
pvs_PSCD153 = [
    "SAROP31-PSCD153:MOTOR_PROBE.RBV"
]

###########################
# Horizontal KB mirror OKBH154
pvs_OKBH154 = [
    "SAROP31-OKBH154:W_X.RBV",
    "SAROP31-OKBH154:W_Y.RBV",
    "SAROP31-OKBH154:W_RX.RBV",
    "SAROP31-OKBH154:W_RY.RBV",
    "SAROP31-OKBH154:W_RZ.RBV",
    "SAROP31-OKBH154:BU.RBV",
    "SAROP31-OKBH154:BD.RBV",
    "SAROP31-OKBH154:TY1.RBV",
    "SAROP31-OKBH154:TY2.RBV",
    "SAROP31-OKBH154:TY3.RBV",
    "SAROP31-OKBH154:TX2.RBV",
]

####################
# Standa motors (mainly used with the X-ray eye)
pvs_standa = [
    "SARES30-MOBI1:MOT_1.RBV",
    "SARES30-MOBI1:MOT_2.RBV",
    "SARES30-MOBI1:MOT_3.RBV",
]

####################
# Newport 300 mm stage
pvs_newport_300 = [
    "SARES30-MOBI1:MOT_5.RBV",
]


###############################
# Smaract stages mini XYZ from SwissMX
pvs_smaract_xyz = [
    "SARES30-MCS2750:MOT_1.RBV",
    "SARES30-MCS2750:MOT_1.VAL",
    "SARES30-MCS2750:MOT_2.RBV",
    "SARES30-MCS2750:MOT_2.VAL",
    "SARES30-MCS2750:MOT_3.RBV",
    "SARES30-MCS2750:MOT_3.VAL",
]

####################
# Attocube motors
pvs_attocube = [
    "SARES30-ATTOCUBE:A0-POS",
    "SARES30-ATTOCUBE:A1-POS",
]

###############################
# Smaract stages from Juraj
pvs_smaract_juraj = [
    "SARES30-XSMA156:X:MOTRBV",
    "SARES30-XSMA156:Y:MOTRBV",
    "SARES30-XSMA156:Z:MOTRBV",
    "SARES30-XSMA156:Ry:MOTRBV",
    "SARES30-XSMA156:Rx:MOTRBV",
    "SARES30-XSMA156:Rz:MOTRBV",
]

pvs_diffractometer_1 = [
    "SARES30-CPCL-ECMC02:ROT2THETA-PosAct",
    "SARES30-CPCL-ECMC02:ROTTHETA-PosAct",
    "SARES30-CPCL-ECMC02:TRXBASE-PosAct",
    "SARES30-CPCL-ECMC02:TRY-PosAct",
    "SARES30-CPCL-ECMC02:TRX-PosAct",
    "SARES30-CPCL-ECMC02:TRZ-PosAct",
    "SARES30-CPCL-ECMC02:TD-PosAct",
]


##########################################################################################################
# Bernina

###################
# Slits OAPU092
pvs_OAPU092 = [
    "SAROP21-OAPU092:MOTOR_X.RBV",
    "SAROP21-OAPU092:MOTOR_Y.RBV",
    "SAROP21-OAPU092:MOTOR_W.RBV",
    "SAROP21-OAPU092:MOTOR_H.RBV",
]

###################
# Bernina mono
pvs_ODCM098 = [
    "SAROP21-ARAMIS:ENERGY_SP",
    "SAROP21-ARAMIS:ENERGY",
    "SAROP21-PBPS103:MOTOR_X1.DRBV",
    "SAROP21-PBPS103:MOTOR_Y1.DRBV",
    "SAROP21-PBPS103:MOTOR_X1.RBV",
    "SAROP21-PBPS103:MOTOR_Y1.RBV",
    "SAROP21-PBPS103:MOTOR_PROBE.RBV",
    "SAROP21-PPRM113:MOTOR_PROBE.RBV"
]

###################
# Beam position and intensity monitor PBPS149
pvs_PBPS103 = [
    "SAROP21-PBPS103:MOTOR_X1.DRBV",
    "SAROP21-PBPS103:MOTOR_Y1.DRBV",
    "SAROP21-PBPS103:MOTOR_X1.RBV",
    "SAROP21-PBPS103:MOTOR_Y1.RBV",
    "SAROP21-PBPS103:MOTOR_PROBE.RBV",
]

###################
# Slits OAPU102
pvs_OAPU102 = [
    "SAROP21-OAPU102:MOTOR_X.RBV",
    "SAROP21-OAPU102:MOTOR_Y.RBV",
    "SAROP21-OAPU102:MOTOR_W.RBV",
    "SAROP21-OAPU102:MOTOR_H.RBV",
]

####################
# Pulse picker OPPI113
pvs_OPPI113 = [
    "SAROP21-OPPI113:MOTOR_X1.RBV",
    "SAROP21-OPPI113:MOTOR_Y1.RBV",
]


##########################################################################################################
# special pvs configurations

####################
pvs_cristallina = (
    pvs_machine
    # + pvs_RF
    # + pvs_undulator
    + pvs_gas_monitor
    + pvs_OAPU044
    + pvs_PBPS053
    + pvs_OATT053
    + pvs_PPRM053
    + pvs_PSSS059
    + pvs_OOMH067
    + pvs_PSCR068
    + pvs_OOMH084
    + pvs_PPRM085
    + pvs_OAPU107
    + pvs_PBPS113
    + pvs_PDIM113 
    + pvs_PPRM113
    + pvs_OLAS147
    + pvs_OAPU149
    + pvs_PBPS149
    + pvs_PPRM150
    + pvs_OATA150
    + pvs_OPPI151
    + pvs_ODMV152
    + pvs_OKBV153
    + pvs_PSCD153
    + pvs_OKBH154
#    + pvs_standa
#    + pvs_smaract_xyz
#    + pvs_Bernina
)


# + pvs_attocube
# + pvs_smaract_juraj


####################
pvs_bernina = (
    pvs_machine
    # + pvs_RF
    # + pvs_undulator
    + pvs_gas_monitor
    + pvs_OAPU044
    + pvs_PBPS053
    + pvs_OATT053
    + pvs_PPRM053
    + pvs_PSSS059
    + pvs_OOMH067
    + pvs_PSCR068
    + pvs_OAPU092
#    + pvs_OOMV092
#    + pvs_PPRM094
#    + pvs_PSCR097
    + pvs_ODCM098
    + pvs_OAPU102
    + pvs_PBPS103
    + pvs_OPPI113
#    + pvs_PPRM113_Bernina
)
