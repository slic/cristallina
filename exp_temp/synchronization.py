import requests
import numpy as np

from loguru import logger


def start_sequence(n: int = 100, pulse_phase: float = np.pi/8):
    parameters = {"n":n, "pulse_phase":pulse_phase}
    url = "http://oscillations.psi.ch:8000/pulse"
    r = requests.get(url, params=parameters)

    d = r.json()
    return d['pids']