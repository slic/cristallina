from slic.core.acquisition import SFAcquisition

from .channels import detectors_with_config, detectors
from .channels import bs_channels_jf_direct_beam, bs_channels_pbps_snapshot
from .channels import pvs_cristallina, pvs_bernina,  bs_channels_DCM_Bernina


instrument = "cristallina"
pgroup = "p21569"

acqui_bill = SFAcquisition(
    instrument,
    pgroup,
    default_channels=bs_channels_pbps_snapshot,
    default_pvs=pvs_cristallina,
    default_detectors=detectors,
    rate_multiplicator=1,
)

